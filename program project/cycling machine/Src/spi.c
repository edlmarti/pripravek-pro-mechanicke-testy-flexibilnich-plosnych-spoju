#include "spi.h"

void spi_setup()
{
    // ensure GPIOs are enabled
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOAEN;
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;

    //GPIOB MODER
    GPIOB->MODER &= ~GPIO_MODER_MODER5; // NCS
    GPIOB->MODER |= GPIO_MODER_MODER5_0; // NCS

    // GPIOA MODER
    GPIOA->MODER &= ~(0
                 |  GPIO_MODER_MODER5 // SCK
    //           |  GPIO_MODER_MODER6 // MISO
                 |  GPIO_MODER_MODER7 // MOSI
                );

    GPIOA->MODER |= 0
                 |  GPIO_MODER_MODER5_0*2 // SCK   AF
    //           |  GPIO_MODER_MODER6_0*2 // MISO  AF
                 |  GPIO_MODER_MODER7_0*2 // MOSI  AF
                 ;
    // GPIOA AFR
    GPIOA->AFR[0] &= ~(0
                  |  GPIO_AFRL_AFRL5
    //            |  GPIO_AFRL_AFRL6
                  |  GPIO_AFRL_AFRL7
                 );

    GPIOA->AFR[0] |= 0
                  |  GPIO_AFRL_AFRL5_0*5
    //            |  GPIO_AFRL_AFRL6_0*5
                  |  GPIO_AFRL_AFRL7_0*5
                  ;

    // GPIOA OSPEEDR
    GPIOA->OSPEEDR |= 0
                   |  GPIO_OSPEEDR_OSPEED5_0*3
    //             |  GPIO_OSPEEDR_OSPEED6_0*3
                   |  GPIO_OSPEEDR_OSPEED7_0*3
                   ;

    //configure SPI
    spi_disable();

    SPI1->CR1 |= 0
              |  SPI_CR1_MSTR    // master
           // |  SPI_CR1_BR_0*2  // fpclk/8
              |  SPI_CR1_SSM     // SSM
              |  SPI_CR1_SSI     // SSI
              ;
}

void spi_enable(void)
{
    SPI1->CR1 |= SPI_CR1_SPE;
}
void spi_disable(void)
{
    SPI1->CR1 &= ~SPI_CR1_SPE;
}

void spi_send(const uint8_t data)
{
    while(!(SPI1->SR & SPI_SR_TXE)); // wait until empty
    *(uint8_t*)&(SPI1->DR) = data;
    while(!(SPI1->SR & SPI_SR_TXE) || (SPI1->SR & SPI_SR_BSY)); // wait until empty and stopped sending data
}
