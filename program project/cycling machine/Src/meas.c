#include "meas.h"

void meas_setup()
{
    // PB8 -> SW
    // PB12 -> LATCH RESET
    // PA15 -> LATCH

    // Ensure GPIOB is enabled
    RCC->AHB1ENR |= 0
                 |  RCC_AHB1ENR_GPIOBEN
                 |  RCC_AHB1ENR_GPIOAEN
                 ;

    //set them both as output
    GPIOB->MODER &= ~(0
                 |  GPIO_MODER_MODER8
                 |  GPIO_MODER_MODER12
                 );

    GPIOB->MODER |= 0
                 |  GPIO_MODER_MODER8_0
                 |  GPIO_MODER_MODER12_0
                 ;

    // ensure PA15 is input
    GPIOA->MODER &= ~GPIO_MODER_MODER15;

}

void meas_switch_to_miliohm()
{
    GPIOB->ODR |= GPIO_ODR_OD8;
}

void meas_switch_to_latch()
{
    GPIOB->ODR &= ~GPIO_ODR_OD8;
}

void meas_latch_set_reset()
{
    GPIOB->ODR &= ~GPIO_ODR_OD12;
}

void meas_latch_set_nreset()
{
    GPIOB->ODR |= GPIO_ODR_OD12;
}

void meas_latch_clear()
{
    meas_latch_set_reset();
    meas_latch_set_nreset();
}

uint8_t meas_latch()
{
    return (GPIOA->IDR & GPIO_IDR_ID15) ?1:0;
}
