#ifndef BUTTONS_H_
#define BUTTONS_H_

#include "system.h"
#include "buzzer.h"
#include "stepper.h"

// PA4  - END    microswitch 0 = normal 1 = activated
// PD2  - HOME   microswitch 0 = normal 1 = activated
// PC0  - STOP   button      1 = normal 0 = activated
// PC1  - START  button      1 = normal 0 = activated


// using landscape mode, so the buttons are rotated 90 degrees clockwise
// therefore
// left => up
// up => right
// right => down
// down => left
// PC8  - CENTER key         1 = normal 0 = activated
// PC9  - UP     key         1 = normal 0 = activated
// PC10 - LEFT   key         1 = normal 0 = activated
// PC11 - DOWN   key         1 = normal 0 = activated
// PC12 - RIGHT  key         1 = normal 0 = activated



// adding two additional states for easier
// detection of transition states
typedef enum
{
    // button is not pressed
    BUTTON_STATE_NONE = 0,
    // button was just pressed (went from 0->1)
    BUTTON_STATE_PRESSED = 1,
    // button was pressed already on the previous check
    // and is therefore considered held
    BUTTON_STATE_HELD = 2,
    // button transitioned state again to the not
    // pressed state - released
    BUTTON_STATE_RELEASED = 3,

    BUTTON_STATE_MAX
}button_state_t;

typedef struct
{
    button_state_t home   : 4;
    button_state_t end    : 4;
    button_state_t start  : 4;
    button_state_t stop   : 4;
    button_state_t left   : 4;
    button_state_t right  : 4;
    button_state_t down   : 4;
    button_state_t up     : 4;
    button_state_t center : 4;
}buttons_t;

void buttons_setup();

buttons_t buttons_get_state();

#endif /* BUTTONS_H_ */
