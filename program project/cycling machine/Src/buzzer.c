#include "buzzer.h"
static const uint32_t prescaler_setting = 100;

// PB2 = buzzer
void buzzer_setup()
{
    RCC->AHB1ENR |=RCC_AHB1ENR_GPIOBEN;

    GPIOB->MODER &= ~GPIO_MODER_MODER2;

    GPIOB->MODER |= GPIO_MODER_MODER2_0*1; //out

    buzzer_turn_off();
    timer_setup(TIM5, 2);

    NVIC_SetPriority(TIM5_IRQn, 15); // set high priority for the interrupt
    NVIC_EnableIRQ(TIM5_IRQn); // enable the interrupt

    timer_PSC(TIM5,prescaler_setting);
    timer_ARR(TIM5,1);
}

void buzzer_toggle()
{
    GPIOB->ODR ^= GPIO_ODR_OD2;
}

void buzzer_turn_off()
{
    GPIOB->ODR &= ~GPIO_ODR_OD2;
}

void buzzer_freq(int32_t freq_mHz)
{
    if(freq_mHz > 0)
    {
        const uint64_t scaled_clock = (sysclk_freq/prescaler_setting)*(1000/4);
        uint64_t arr_val = scaled_clock/freq_mHz;
        timer_ARR(TIM5,arr_val);
    }
    else
    {
        timer_ARR(TIM5,1);
    }
}

void buzzer_start()
{
    timer_enable(TIM5);
}

void buzzer_stop()
{
    timer_disable(TIM5);
    buzzer_turn_off();
}

void TIM5_IRQHandler()
{
    buzzer_toggle();
    TIM5->SR &= ~TIM_SR_UIF;
}

#define TRANSPOSE_MUL 2
void buzzer_melody(const int32_t* note_freqs_mHz, uint32_t note_count, uint32_t note_time_ms)
{
    for(uint32_t i = 0; i < note_count; i++)
    {
        buzzer_freq(note_freqs_mHz[i]*TRANSPOSE_MUL);
        buzzer_start();
        for_delay(note_time_ms*1000);
        buzzer_stop();
    }
    buzzer_turn_off();
}

void buzzer_melody_advanced(const int32_t* note_freqs_mHz, const uint32_t * note_durations, uint32_t note_count, uint32_t note_time_ms)
{
    for(uint32_t i = 0; i < note_count; i++)
    {
        buzzer_freq(note_freqs_mHz[i]*TRANSPOSE_MUL);
        buzzer_start();
        for_delay(note_time_ms*1000/note_durations[i]);
        buzzer_stop();
    }
    buzzer_turn_off();
}
