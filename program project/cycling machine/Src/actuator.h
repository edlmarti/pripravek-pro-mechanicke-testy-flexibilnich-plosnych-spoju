#ifndef ACTUATOR_H_
#define ACTUATOR_H_

#include <stdlib.h>
#include <math.h>

#include "system.h"
#include "stepper.h"
#include "timers.h"

void act_setup();

// prepares and allocates the lookup table for step durations for smooth
// harmonic motion
// returns 0 if done without a problem
int act_calculate_timestep_table(uint64_t span_um, uint64_t cycle_time_ms,
        uint64_t steps_per_m);

// perform a single cycle
void act_cycle();

// free the table data
void act_free_table();

#endif /* ACTUATOR_H_ */
