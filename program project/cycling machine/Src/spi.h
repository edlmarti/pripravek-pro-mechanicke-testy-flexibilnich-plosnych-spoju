#ifndef SPI_H_
#define SPI_H_

#include "system.h"

// setup SPI1
void spi_setup();

// enable SPI
void spi_enable(void);

// disable SPI
void spi_disable(void);

// send a single byte of data
void spi_send(const uint8_t data);


#endif /* SPI_H_ */
