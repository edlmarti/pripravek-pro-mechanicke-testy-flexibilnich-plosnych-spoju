#include "display.h"

const rgb_t color_red   = {.r = 0xff, .g = 0x00, .b = 0x00};
const rgb_t color_green = {.r = 0x00, .g = 0xff, .b = 0x00};
const rgb_t color_blue  = {.r = 0x00, .g = 0x00, .b = 0xff};
const rgb_t color_black = {.r = 0x00, .g = 0x00, .b = 0x00};
const rgb_t color_white = {.r = 0xff, .g = 0xff, .b = 0xff};

// foreground color
rgb_t color_fg = color_white;
// background color
rgb_t color_bg = color_black;

void display_setup()
{
    // setup the SPI
    spi_setup();

    // ensure GPIOs are enabled
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOAEN;

    //setup DCX pin
    GPIOB->MODER &= ~GPIO_MODER_MODER3 ;// DCX
    GPIOB->MODER |= GPIO_MODER_MODER3_0*1; // DCX

    GPIOA->MODER &= ~GPIO_MODER_MODER1 ;// DCX
    GPIOA->MODER |= GPIO_MODER_MODER1_0*1; // DCX

    GPIOA->ODR &= ~GPIO_ODR_OD1;  //nRESET=0
    for_delay(150000);
    GPIOA->ODR |= GPIO_ODR_OD1;  //nRESET=1
    for_delay(150000);

    GPIOB->ODR &= ~GPIO_ODR_OD5; // nCS=0
    spi_enable();

    // MADCTL
    send_command(0x36);
    // D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 |
    // MY | MX | MV | ML | BGR| MH | 0  | 0  |
    //  1 |  1 |  1 |  0 |  1 |  0 |  0 |  0 |
    //
    // MY, MX, MV = 1 -> landscape mode
    send_data(0b11101000);

    // COLMOD
    send_command(0x3A);
    // D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 |
    // MY |      DPI     | 0  |      DBI     |
    //  0 |  1 |  1 |  0 |  0 |  1 |  1 |  0 |
    //
    // 110 -> using the easier/slower  18b pixel (6b/6b/6b)
    send_data(0b01100110);

}

void display_sleep()
{
    send_command(0x10);
    for_delay(10000);

}
void display_wake_up()
{
    send_command(0x11);
    for_delay(10000);
}

void display_on()
{
    send_command(0x29);

}
void display_off()
{
    send_command(0x28);
}

void send_command(uint8_t command)
{
    display_set_DC_command();
    spi_send(command);
    display_set_DC_data();
}

void send_data(uint8_t data)
{
    display_set_DC_data();
    spi_send(data);
}

void send_data_16b(uint16_t data)
{
    display_set_DC_data();
    spi_send(data>>8);
    spi_send(data&0xFF);
}

void display_set_DC_command()
{
    GPIOB->ODR &= ~GPIO_ODR_OD3;
}

void display_set_DC_data()
{
    GPIOB->ODR |= GPIO_ODR_OD3;
}


#define LETTER_ROWS 7
#define LETTER_COLS 5

uint16_t draw_letter_7x5(char ch, uint16_t posx, uint16_t posy, uint16_t scale)
{

    if(' ' > ch || ch > 0x7f) return 0;


    send_command(0x2A);
    send_data_16b(posx);
    posx+=LETTER_COLS*scale-1;
    send_data_16b(posx);

    send_command(0x2B);
    send_data_16b(posy);
    posy+=LETTER_ROWS*scale-1;
    send_data_16b(posy);
    send_command(0x2C);
    for(uint8_t r = 0; r < LETTER_ROWS*scale;r++)
    {
        for(uint8_t c = 0; c < LETTER_COLS*scale;c++)
        {

            send_data((font_5x7(ch,r/scale)&0x01<<(LETTER_COLS-c/scale-1)) ? color_fg.r : color_bg.r);
            send_data((font_5x7(ch,r/scale)&0x01<<(LETTER_COLS-c/scale-1)) ? color_fg.g : color_bg.g);
            send_data((font_5x7(ch,r/scale)&0x01<<(LETTER_COLS-c/scale-1)) ? color_fg.b : color_bg.b);
        }
    }
    send_command(0x00);
    return LETTER_COLS*scale;
}

void draw_window(uint16_t posx, uint16_t posy,uint16_t tox, uint16_t toy)
{
    send_command(0x2A);
    send_data_16b(posx);
    send_data_16b(tox);
    send_command(0x2B);
    send_data_16b(posy);
    send_data_16b(toy-1);
    send_command(0x2C);

    for(uint16_t c =posx; c < tox+1;c++)
    {
        send_data(color_fg.r);
        send_data(color_fg.g);
        send_data(color_fg.b);
    }
    for(uint16_t r =posy+1; r < toy-1;r++)
    {
        send_data(color_fg.r);
        send_data(color_fg.g);
        send_data(color_fg.b);
        for(uint16_t c =posx+1; c < tox;c++)
        {
            send_data(color_bg.r);
            send_data(color_bg.g);
            send_data(color_bg.b);
        }
        send_data(color_fg.r);
        send_data(color_fg.g);
        send_data(color_fg.b);
    }
    for(uint16_t c =posx; c < tox+1;c++)
    {
        send_data(color_fg.r);
        send_data(color_fg.g);
        send_data(color_fg.b);
    }
    send_command(0x00);
}

void draw_textbox(char* str, uint16_t posx, uint16_t posy, uint16_t tox, uint16_t spacingx, uint16_t spacingy, uint16_t scale)
{
    uint16_t letterx = posx;
    uint16_t lettery = posy;
    for(uint16_t i = 0;; i++)
    {
        if(str[i] !='\0')
        {
            letterx += draw_letter_7x5(str[i], letterx, lettery,scale) + spacingx;
            if((letterx > tox-7 )|| (str[i] == '\n'))
            {
                lettery += LETTER_ROWS*scale + spacingy;
                letterx  = posx;
            }
        }
        else
            return;
    }
}

void display_clear()
{
    send_command(0x2A);
    send_data(0x00);
    send_data(0x00);

    send_data(319>>8);
    send_data(319&0xFF);
    send_command(0x2B);
    send_data(0x00);
    send_data(0x00);
    send_data(239>>8);
    send_data(239&0xFF);
    send_command(0x2C);
    for (uint32_t tft_iter = 0; tft_iter < (320*240); ++tft_iter)
    {
       // Write a 16-bit color.
           send_data(0x00);
           send_data(0x00);
           send_data(0x00);
    }

}


