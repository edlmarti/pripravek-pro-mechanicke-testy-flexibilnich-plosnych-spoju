#ifndef STEPPER_H_
#define STEPPER_H_

#include "timers.h"
#include "system.h"

// setup the pins and the duty cycle limit
void stepper_setup(uint32_t resistace_mOhm, uint32_t max_current_mA,int32_t supply_voltage_mV);

// set the duty cycle according to max current 1000 -> 100% of the max value allowed by electrical parameters
void stepper_current_throttle(uint32_t percentage_of_maxx1000);

// set the position counter to 0
void stepper_set_home_position();

// do a step forward
void stepper_forward();

// do a step backwards
void stepper_back();

// pull all the motor pins to ground
void stepper_current_off();

// run the timer, sending the duty cycle to motor
void stepper_on();

// stop the timer, no current is sent to the motor
void stepper_off();

// get the current value of the step counter
int32_t stepper_pos();

// set outputs to mirror current step
void stepper_set_outputs();

#endif /* STEPPER_H_ */
