#ifndef MEAS_H_
#define MEAS_H_

#include <stdint.h>
#include <stm32f4xx.h>
#include "system.h"

// setup all the pins
void meas_setup();

// switch analog swithes to miliohmmeter
void meas_switch_to_miliohm();

// switch analog switches to latch
void meas_switch_to_latch();

// set the latch reset pin to the reset value
void meas_latchs_set_reset();

// set the latch reset pin to the nreset value
void meas_latch_set_nreset();

// reset the latch contents
void meas_latch_clear();

// get the latch value
uint8_t meas_latch();


#endif /* MEAS_H_ */
