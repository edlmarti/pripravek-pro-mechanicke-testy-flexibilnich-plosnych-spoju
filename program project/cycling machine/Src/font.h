#ifndef FONT_H_
#define FONT_H_

#include <stdint.h>

// simple 5x7 font
uint8_t font_5x7(char ch, uint8_t row);

#endif /* FONT_H_ */
