#include "system.h"

const uint64_t sysclk_freq = 16000000;
system_state_t system_state = STATE_STARTING;
system_error_t system_error = ERR_UNKNOWN;

system_param_t system_parms[PARAM_COUNT] =
{
    //    name          value  maxval  digits  dpoint unit   desc
        {"Bend method", 0    , 1     , 1     , 0     , "" , "desc:\n0 = rolling;\n1 = push-to-flex"},
        {"Cycle set." , 0    , 99999 , 5     , 0     , "" , "desc:\n0 = to discont.;\nany other =\nnumber of\nbend cycles to do"},
        {"Torque set.", 700  , 1000  , 4     , 1     , "%", "desc:\nOffset used in\npush-to-flex\n(in mm)"},
        {"Pos. calib.", 5000 , 9999  , 4     , 0     , "/m","desc:\nNumber of steps\nin a meter"},
        {"Bend span"  , 250  , 1600  , 4     , 1     , "mm","desc:\nSpan of bending\ncycle (in mm)"},
        {"Bend offset", 0    , 1600  , 4     , 1     , "mm","desc:\nOffset used in\npush-to-flex\n(in mm)"},
        {"Cycle time" , 600  , 9999  , 4     , 2     , "s", "desc:\nDuration of\na single cycle"}
};

void for_delay(uint32_t us)
{
    // 7 instructions per loop
    // 4 clock cycles per instruction
    // -> 28 clock cycles for one loop
    uint32_t loops = ((sysclk_freq/1000000)*us)/32;
    for(uint32_t i = 0; i < loops; i++);
}

void enable_fpu()
{
    SCB->CPACR |= ((3UL << 10*2)|(3UL << 11*2));
}


