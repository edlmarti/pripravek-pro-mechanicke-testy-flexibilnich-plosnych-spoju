#include "buttons.h"


void buttons_setup()
{
    // ensure GPIOs are enabled
    RCC->AHB1ENR |= 0
                 |  RCC_AHB1ENR_GPIOAEN
                 |  RCC_AHB1ENR_GPIOCEN
                 |  RCC_AHB1ENR_GPIODEN
                 ;

    // ensure all buttons are set as inputs (clear MODER)
    GPIOA->MODER &= ~(0
                 |  GPIO_MODER_MODER4
                 );
    GPIOC->MODER &= ~(0
                 |  GPIO_MODER_MODER0
                 |  GPIO_MODER_MODER1
                 |  GPIO_MODER_MODER8
                 |  GPIO_MODER_MODER9
                 |  GPIO_MODER_MODER10
                 |  GPIO_MODER_MODER11
                 |  GPIO_MODER_MODER12
                 );
    GPIOD->MODER &= ~(0
                 |  GPIO_MODER_MODER2
                 );


    // setup the interrupts
    // enable clock for SYSCFGEN
    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;

    // choose port C for EXTI0
    // choose port C for EXTI1
    // choose port D for EXTI2
    // choose port A for EXTI4
    SYSCFG->EXTICR[0] |= 0
                      |  SYSCFG_EXTICR1_EXTI0_PC
                      |  SYSCFG_EXTICR1_EXTI1_PC
                      |  SYSCFG_EXTICR1_EXTI2_PD
                      ;
    SYSCFG->EXTICR[1] |= SYSCFG_EXTICR2_EXTI4_PA;

    // interrupt masks
    EXTI->IMR |= 0
              |  EXTI_IMR_MR0
              |  EXTI_IMR_MR1
              |  EXTI_IMR_MR2
              |  EXTI_IMR_MR4
              ;
    // event masks
    EXTI->IMR |= 0
//            |  EXTI_IMR_MR0
//            |  EXTI_IMR_MR1
//            |  EXTI_IMR_MR2
//            |  EXTI_IMR_MR4
              ;

    // setup rising edge trigger for the external buttons
    // and falling edge for the endstop microswitches
    EXTI->FTSR |= EXTI_FTSR_TR0; // enable falling edge trigger
    EXTI->FTSR |= EXTI_FTSR_TR1; // enable falling edge trigger
    EXTI->RTSR |= EXTI_FTSR_TR2; // enable rising edge trigger
    EXTI->RTSR |= EXTI_FTSR_TR4; // enable rising edge trigger

    // set high priority for the interrupts
    // start < home < end < stop
    NVIC_SetPriority(EXTI0_IRQn, 5); // stop
    NVIC_EnableIRQ(EXTI0_IRQn);
    NVIC_SetPriority(EXTI1_IRQn, 8); // start
    NVIC_EnableIRQ(EXTI1_IRQn);
    NVIC_SetPriority(EXTI2_IRQn, 7); // home
    NVIC_EnableIRQ(EXTI2_IRQn);
    NVIC_SetPriority(EXTI4_IRQn, 6); // end
    NVIC_EnableIRQ(EXTI4_IRQn);
}

button_state_t create_button_state(uint32_t prev_state, uint32_t new_state)
{
    uint32_t combined = ((prev_state<<1)| new_state)&0x03;
    static const button_state_t statemap[] =
    {
        BUTTON_STATE_NONE,
        BUTTON_STATE_PRESSED,
        BUTTON_STATE_RELEASED,
        BUTTON_STATE_HELD
    };
    return statemap[combined];
}

buttons_t buttons_get_state()
{

     // 0 = normal state, 1 = activated state
     static uint32_t prev_home   = 0;
     static uint32_t prev_end    = 0;
     static uint32_t prev_start  = 0;
     static uint32_t prev_stop   = 0;
     static uint32_t prev_left   = 0;
     static uint32_t prev_right  = 0;
     static uint32_t prev_down   = 0;
     static uint32_t prev_up     = 0;
     static uint32_t prev_center = 0;

     uint32_t home   = (GPIOD->IDR & GPIO_IDR_ID2 )>0;
     uint32_t end    = (GPIOA->IDR & GPIO_IDR_ID4 )>0;
     uint32_t start  = !((GPIOC->IDR & GPIO_IDR_ID1 )>0);
     uint32_t stop   = !((GPIOC->IDR & GPIO_IDR_ID0 )>0);
     uint32_t left   = !((GPIOC->IDR & GPIO_IDR_ID10)>0);
     uint32_t right  = !((GPIOC->IDR & GPIO_IDR_ID12)>0);
     uint32_t down   = !((GPIOC->IDR & GPIO_IDR_ID11)>0);
     uint32_t up     = !((GPIOC->IDR & GPIO_IDR_ID9 )>0);
     uint32_t center = !((GPIOC->IDR & GPIO_IDR_ID8 )>0);

    buttons_t this_check =
    {
       .home   = create_button_state(prev_home  , home  ),
       .end    = create_button_state(prev_end   , end   ),
       .start  = create_button_state(prev_start , start ),
       .stop   = create_button_state(prev_stop  , stop  ),
       .left   = create_button_state(prev_left  , left  ),
       .right  = create_button_state(prev_right , right ),
       .down   = create_button_state(prev_down  , down  ),
       .up     = create_button_state(prev_up    , up    ),
       .center = create_button_state(prev_center, center),
    };

    prev_home   = home;
    prev_end    = end;
    prev_start  = start;
    prev_stop   = stop;
    prev_left   = left;
    prev_right  = right;
    prev_down   = down;
    prev_up     = up;
    prev_center = center;
    return this_check;
}

// stop button interrupt
void EXTI0_IRQHandler(void)
{
    stepper_off();
    system_state = STATE_ERROR;
    system_error = ERR_SHUTDOWN_BUTTON;

    EXTI->PR |= EXTI_PR_PR0;
}

// start button
void EXTI1_IRQHandler(void)
{
    EXTI->PR |= EXTI_PR_PR1;
    switch(system_state)
    {
    case(STATE_SETUP):
    {
        system_state = STATE_PARAM_SAVE;
        break;
    }
    case(STATE_END):
    {
        system_state = STATE_STARTING;
        break;
    }
    case(STATE_SAMPLE_INSERT):
    {
        stepper_off();
        system_state = STATE_TESTING;
        for_delay(2000000);
        break;
    }
    default:
    {
        break;
    }
    }

}

// home
void EXTI2_IRQHandler(void)
{
    switch(system_state)
    {
    case(STATE_FINDING_HOME):
    {
        system_state = STATE_HOME_FOUND;

        break;
    }
    default:
    {
        system_state = STATE_ERROR;
        system_error = ERR_HOMESTOP;
        break;
    }
    }
    EXTI->PR |= EXTI_PR_PR2;
}

// end
void EXTI4_IRQHandler(void)
{
    system_state = STATE_ERROR;
    system_error = ERR_ENDSTOP;
    EXTI->PR |= EXTI_PR_PR4;
}

