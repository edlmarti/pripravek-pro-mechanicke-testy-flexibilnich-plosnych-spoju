#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <stdint.h>
#include <stm32f4xx.h>

extern const uint64_t sysclk_freq;

void for_delay(uint32_t us);

void enable_fpu();

typedef enum
{
    // booting up
    STATE_STARTING = 0,

    // setting up variables
    STATE_SETUP,
    // user is editing a variable
    STATE_EDITING,
    // save and compute params
    STATE_PARAM_SAVE,
    // single cycle setup before homing
    STATE_HOMING_SETUP,
    // carrige is moved until it hits home endstop
    STATE_FINDING_HOME,
    // carrige hit home endstop
    STATE_HOME_FOUND,
    // wait for the sample to be inserted
    STATE_SAMPLE_INSERT,
    // performing the test
    STATE_TESTING,
    // test ended
    STATE_END,

    // something bad happened
    STATE_ERROR,

    STATE_MAX
} system_state_t;

extern system_state_t system_state;

typedef enum
{
    ERR_UNKNOWN = 0,
    ERR_SHUTDOWN_BUTTON = 1,
    ERR_BADSTEP = 2,
    ERR_ENDSTOP = 3,
    ERR_HOMESTOP = 4,

    ERR_MAX
} system_error_t;
extern system_error_t system_error;

typedef struct
{
    char * name;
    uint32_t value;
    uint32_t max_value;
    uint32_t digits;
    uint32_t digits_behind_decimal;
    char * unit;
    char * description;
}system_param_t;

#define PARAM_COUNT 7
extern system_param_t system_parms[PARAM_COUNT];

#define PARAM_METHOD 0
#define PARAM_CYCLES 1
#define PARAM_TORQUE 2
#define PARAM_STEPS  3
#define PARAM_SPAN   4
#define PARAM_OFFSET 5
#define PARAM_TIME   6

#endif /* SYSTEM_H_ */
