#include "actuator.h"


void act_setup()
{
    stepper_setup(2000, 1400, 12000);
    stepper_current_off();

    timer_setup(TIM2,2);
    TIM2->DIER &= ~TIM_DIER_UIE; // disable update interrupt
    //NVIC_SetPriority(TIM2_IRQn, 21); // set high priority for the interrupt
    //NVIC_EnableIRQ(TIM2_IRQn); // enable the interrupt
    timer_onepulse(TIM2, 2);
}

#define ACTUATOR_LENGTH_UM 300000

uint32_t *timestep_table;
uint32_t timestep_table_size;
uint16_t tim2_presc;

int act_calculate_timestep_table(uint64_t span_um, uint64_t cycle_time_ms, uint64_t steps_per_m)
{
    //how many steps are needed to get to the furthest point
    timestep_table_size = ((uint64_t)(span_um)*(uint64_t)(steps_per_m))/1000000;
    // allocate the space required by
    timestep_table = (uint32_t*)malloc( timestep_table_size* sizeof(uint32_t));

    if(!timestep_table) {return 1;} // malloc failed

    float delta_x = 1.f/((float)steps_per_m);
    float R = ((float)span_um)/1000000.f;
    float C = (1.f/((float)cycle_time_ms))*1000;

    float delta_t0 = acos(1-(2*delta_x)/(R))/(2 *M_PI *C);

    tim2_presc = (uint16_t)((((float)sysclk_freq)*(delta_t0))/(4*0x7FFE));

    if(tim2_presc<2) {tim2_presc=2;}

    timer_PSC(TIM2, tim2_presc);
    //uint32_t dbg_table[256];

    for(uint32_t i = 0; i < timestep_table_size; i++)
    {
        float x1 = delta_x*i;
        float delta_t = (acos(1-(2*(delta_x+x1))/(R)) - acos(1-(2*x1)/(R)))/(2 *M_PI *C);
        uint16_t tim2_arr = (uint16_t)((((float)sysclk_freq)*(delta_t))/(4*(tim2_presc-1)));
        //timestep_table[i] = (uint32_t)(delta_t*1000000);
        timestep_table[i] = tim2_arr;
        //dbg_table[i] = (uint32_t)(delta_t*1000000);
    }

    return 0;
}


void act_cycle()
{
    stepper_on();
    for(uint32_t i = 0; i<timestep_table_size; i++)
    {
        if(system_state == STATE_ERROR)
            break;
        timer_ARR(TIM2, timestep_table[i]);
        timer_enable(TIM2);
        while(!(TIM2->SR & TIM_SR_UIF));
        TIM2->SR &= ~TIM_SR_UIF;

        //for_delay(timestep_table[i]);
        stepper_forward();

    }
    for(uint32_t i = 0; i<timestep_table_size; i++)
    {
        if(system_state == STATE_ERROR)
            break;
        timer_ARR(TIM2, timestep_table[i]);
        timer_enable(TIM2);
        while(!(TIM2->SR & TIM_SR_UIF));
        TIM2->SR &= ~TIM_SR_UIF;

        //for_delay(timestep_table[i]);
        stepper_back();
    }
    stepper_off();
    stepper_current_off();

}

void act_free_table()
{
    free(timestep_table);
    timestep_table = NULL;
}
