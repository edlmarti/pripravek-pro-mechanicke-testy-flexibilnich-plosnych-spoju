#include <stdint.h>
#include <stm32f4xx.h>
#include <stdio.h>
#include <math.h>

#include "timers.h"
#include "buzzer.h"
#include "buttons.h"
#include "stepper.h"
#include "meas.h"
#include "display.h"
#include "actuator.h"
#include "system.h"

uint32_t ten_to(uint32_t num)
{
    uint32_t ret = 1;
    for(uint32_t i = 0; i < num; i++)
    {
        ret*=10;
    }
    return ret;
}

int main(void)
{
    color_bg = color_blue;
    color_fg = color_white;
    static const int32_t error_melody[] = {NOTE_A4, NOTE_F4};
    static const int32_t start_melody[] = {NOTE_C4, NOTE_REST, NOTE_E4, NOTE_G4};
    static const int32_t end_melody[] = {NOTE_C4, NOTE_REST, NOTE_G4, NOTE_E4};
    static const int32_t beep[] = {NOTE_C4};
    char str[256];
    uint32_t cycles = 0;

    enable_fpu();
    buttons_setup();
    buzzer_setup();
    // 240x320
    display_setup();
    stepper_off();
    meas_setup();
    //send_command(0x21); // color inversion
    display_wake_up();
    act_setup();
    for_delay(10000);

    buttons_t buttons = buttons_get_state();;
    uint32_t selected = 0;
    int32_t edit_pointer = 0;
    int32_t edited_val;
    uint32_t stepper_hack = 0;

    while(1)
    {
        buttons = buttons_get_state();
        switch(system_state)
        {
        case(STATE_STARTING):
        {
            cycles = 0;
            display_off();
            display_clear();
            display_on();
            draw_window(20,20,300,220);

            // parameters box
            draw_window(20,20,180,165);
            // state box
            draw_window(20,165,180,220);
            // edit box
            draw_window(180,20,300,165);
            // status box
            draw_window(180,165,300,220);

            draw_letter_7x5('>', 25, 25+10*selected, 1);

            for(uint32_t i = 0; i < PARAM_COUNT; i++)
            {
                draw_textbox(system_parms[i].name,35,25+i*10,220,1,1,1);
                draw_textbox(system_parms[i].unit,160,25+i*10,175,1,1,1);
            }

            draw_textbox("Latch status:",35,170,220,1,1,1);
            draw_textbox("System state:",35,180,220,1,1,1);
            draw_textbox("Cycles:"      ,35,190,220,2,2,2);
            system_state = STATE_SETUP;
            break;
        }
        case(STATE_SETUP):
        {
            uint32_t prev_selected = selected;

            if      (buttons.down == BUTTON_STATE_PRESSED) // || buttons.down == BUTTON_STATE_HELD)
                selected++;
            else if (buttons.up == BUTTON_STATE_PRESSED) // || buttons.up == BUTTON_STATE_HELD)
                selected--;
            // edit value
            else if ((buttons.right == BUTTON_STATE_PRESSED) || (buttons.center == BUTTON_STATE_PRESSED))
            {
                edit_pointer = 0;
                edited_val = system_parms[selected].value;
                draw_letter_7x5('v', 190+12*edit_pointer, 25, 2);
                draw_textbox(system_parms[selected].description,190,85,295,1,1,1);
                system_state = STATE_EDITING;
            }

            selected%=PARAM_COUNT;

            if(prev_selected != selected)
            {
                draw_letter_7x5(' ', 25, 25+10*prev_selected, 1);
                draw_letter_7x5('>', 25, 25+10*selected, 1);
            }

            for(uint32_t i = 0; i < PARAM_COUNT; i++)
            {

                //uint32_t before_per = 0, after_per = 0;
                //hacky solution
                if(system_parms[i].digits_behind_decimal ==0)
                {
                    sprintf(str, "%u", (unsigned int)system_parms[i].value);
                }
                else if(system_parms[i].digits_behind_decimal ==1)
                {
                    sprintf(str, "%u,%u", (unsigned int)system_parms[i].value/10, (unsigned int)system_parms[i].value%10);
                }
                else if(system_parms[i].digits_behind_decimal ==2)
                {
                    sprintf(str, "%u,%u", (unsigned int)system_parms[i].value/100, (unsigned int)system_parms[i].value%100);
                }

                //sprintf(str, "%u", system_parms[i].value);
                draw_textbox(str,130,25+i*10,220,1,1,1);
            }

            break;

        }
        case(STATE_EDITING):
        {

            uint32_t prev_edit_pointer = edit_pointer;
            // save and exit
            if(buttons.center == BUTTON_STATE_PRESSED)
            {
                system_parms[selected].value = edited_val;
                system_state = STATE_SETUP;
                draw_window(180,20,300,165);
                break;
            }
            else if (buttons.left == BUTTON_STATE_PRESSED)
            {
                edit_pointer--;
            }
            else if (buttons.right == BUTTON_STATE_PRESSED)
            {
                edit_pointer++;
            }
            else if (buttons.up == BUTTON_STATE_PRESSED)
            {
                edited_val += ten_to(system_parms[selected].digits-edit_pointer-1);
            }
            else if (buttons.down == BUTTON_STATE_PRESSED)
            {
                edited_val -= ten_to(system_parms[selected].digits-edit_pointer-1);
            }

            if(edited_val < 0)
                edited_val = 0;
            if(edited_val > system_parms[selected].max_value)
                edited_val = system_parms[selected].max_value;

            // exit without saving
            if(edit_pointer < 0)
            {
                system_state = STATE_SETUP;
                draw_window(180,20,300,165);
                break;
            }

            if(edit_pointer >= system_parms[selected].digits)
                edit_pointer = 0;


            // draw number
            //uint32_t before_per = 0, after_per = 0;
            //very hacky solution

            switch(selected)
            {
            case(PARAM_METHOD):
            {
                if(edited_val)
                    sprintf(str, "PTF");
                else
                    sprintf(str, "IPC");
                break;
            }
            case(PARAM_CYCLES):
            {
                sprintf(str, "%05u x", (unsigned int)edited_val);
                break;
            }
            case(PARAM_TORQUE):
            {
                sprintf(str, "%03u,%u %%", (unsigned int)edited_val/10, (unsigned int)edited_val%10);
                break;
            }
            case(PARAM_STEPS):
            {
                sprintf(str, "%04u /m", (unsigned int)edited_val);
                break;
            }
            case(PARAM_SPAN):
            {
                sprintf(str, "%03u,%u mm", (unsigned int)edited_val/10, (unsigned int)edited_val%10);
                break;
            }
            case(PARAM_OFFSET):
            {
                sprintf(str, "%03u,%u mm", (unsigned int)edited_val/10, (unsigned int)edited_val%10);
                break;
            }
            case(PARAM_TIME):
            {
                sprintf(str, "%02u,%02u s", (unsigned int)edited_val/100, (unsigned int)edited_val%100);
                break;
            }
            default:
            {
                sprintf(str, "err");
                break;
            }
            }

            draw_textbox(str,190,45,300,2,2,2);

            //draw the pointer
            if(edit_pointer != prev_edit_pointer)
            {
                uint32_t drawn_ep = edit_pointer, drawn_pep = prev_edit_pointer;
                if(system_parms[selected].digits_behind_decimal)
                {
                    if(edit_pointer >= system_parms[selected].digits - system_parms[selected].digits_behind_decimal)
                        drawn_ep++;
                    if(prev_edit_pointer >= system_parms[selected].digits - system_parms[selected].digits_behind_decimal)
                        drawn_pep++;
                }
                draw_letter_7x5(' ', 190+12*drawn_pep, 25, 2);
                draw_letter_7x5('v', 190+12*drawn_ep, 25, 2);
            }

            break;
        }
        case(STATE_PARAM_SAVE):
        {
            draw_letter_7x5(' ', 25, 25+10*selected, 1);

            meas_switch_to_latch();
            // TBD figure out why the calculation is wrong
            act_calculate_timestep_table(
                    system_parms[PARAM_SPAN].value*100,
                    system_parms[PARAM_TIME].value*4*10,
                    system_parms[PARAM_STEPS].value
                    );
            //stepper_setup(2000, 1400, 12000);
            //TBD fix torque setting
            //stepper_current_throttle(100);//system_parms[PARAM_TORQUE].value);
            for_delay(200000);
            system_state = STATE_HOMING_SETUP;
            break;

        }
        case(STATE_HOMING_SETUP):
        {
            meas_latch_clear();
            buzzer_melody(start_melody, sizeof(start_melody)/sizeof(start_melody[0]), 1000);
            stepper_on();
            system_state = STATE_FINDING_HOME;
            break;
        }
        case(STATE_FINDING_HOME):
        {
            if((GPIOD->IDR & GPIO_IDR_ID2 )>0)
                system_state = STATE_HOME_FOUND;
            for_delay(20000);
            stepper_back();
            break;
        }
        case(STATE_HOME_FOUND):
        {
            // move couple steps out of the switch hystersis
            do
            {
                for_delay(20000);
                stepper_forward();
            }while((GPIOD->IDR & GPIO_IDR_ID2 )>0);

            // move out of the way from the switch and tto the scale  0 mark
            for(uint32_t i = 0; i < 11; i++)
            {
                for_delay(20000);
                stepper_forward();
            }

            // if PTF rather than IPC mode is selected, offset the carrige
            if(system_parms[PARAM_METHOD].value > 0)
            {
                buzzer_melody(beep, sizeof(beep)/sizeof(beep[0]), 1000);

                uint32_t steps_to_do = ((system_parms[PARAM_OFFSET].value/10)*system_parms[PARAM_STEPS].value)/1000;
                for(uint32_t i = 0; i < steps_to_do; i++)
                {
                    for_delay(20000);
                    stepper_forward();
                }
            }

            stepper_off();
            system_state = STATE_SAMPLE_INSERT;
            stepper_on();
            for_delay(2000000);
            break;
        }
        case(STATE_SAMPLE_INSERT):
        {
            // lock in position

            /*
            // pwm on pwm
            if(stepper_hack <= 0)
            {
                stepper_on();
                stepper_hack++;
            }
            else if(stepper_hack <= 1)
            {
                stepper_off();
                stepper_hack++;
            }
            else
                stepper_hack = 0;
            */
            break;
        }
        case(STATE_TESTING):
        {
            act_cycle();
            cycles++;
            if(system_parms[PARAM_CYCLES].value == 0 ? meas_latch() == 1 : system_parms[PARAM_CYCLES].value <= cycles)
            {
                system_state = STATE_END;
                sprintf(str, "END");
                draw_textbox(str,185,190,295,2,2,2);
                buzzer_melody(end_melody, sizeof(end_melody)/sizeof(end_melody[0]), 1000);
            }
            for_delay(3000000);
            break;
        }
        case(STATE_END):
        {
            meas_latch_clear();
            break;
        }
        case(STATE_ERROR):
        {
            color_fg = color_red;
            sprintf(str, "err: %i", system_error);
            draw_textbox(str,185,190,295,2,2,2);
            color_fg = color_white;
            buzzer_melody(error_melody, sizeof(error_melody)/sizeof(error_melody[0]), 1000);

            goto out_of_loop;
        }
        default:
        {
            break;
        }
        }


        sprintf(str, "%i", meas_latch());
        draw_textbox(str,130,170,220,1,1,1);
        sprintf(str, "%i", system_state);
        draw_textbox(str,130,180,220,1,1,1);
        sprintf(str, "%u", (unsigned int)cycles);
        draw_textbox(str,130,190,220,2,2,2);
/*
        draw_textbox("               c  l  r  u  d  st sp ed hm",35,55,300,1,1,1);
        sprintf(str, "buttons:       %u  %u  %u  %u  %u   %u  %u  %u  %u",
                buttons.center,
                buttons.left,
                buttons.right,
                buttons.up,
                buttons.down,
                buttons.start,
                buttons.stop,
                buttons.end,
                buttons.home
        );
        draw_textbox(str,35,65,300,1,1,1);
*/

    }
out_of_loop:
    stepper_off();
	act_free_table();
}
