#ifndef BUZZER_H_
#define BUZZER_H_

#include <stdint.h>
#include <stm32f4xx.h>
#include "system.h"
#include "timers.h"

static const uint32_t prescaler_setting;

// PB2 = buzzer
// setup buzzer pins and TIM5 timer
void buzzer_setup();

// toggle buzzer pin to create sound
void buzzer_toggle();

// set low logic level on the control pin (so that no prolonged DC current flows through the speaker)
void buzzer_turn_off();

// sets the frequency of the buzzer
// use -1 to set it to the maximum (unhearable) frequency (can be used as rest in melodies)
void buzzer_freq(int32_t freq_mHz);

// start buzzing
void buzzer_start();
// stop buzzing
void buzzer_stop();

// play a one voice simple tempo melody
void buzzer_melody(const int32_t* note_freqs_mHz, uint32_t note_count, uint32_t note_time_ms);

// play a one voice variable tempo melody
void buzzer_melody_advanced(const int32_t * note_freqs_mHz, const uint32_t * note_durations, uint32_t note_count, uint32_t note_time_ms);

#define NOTE_FULL 1
#define NOTE_HALF 2
#define NOTE_QUARTER 4
#define NOTE_EIGTH 8
#define NOTE_SIXTEENTH 16

#define NOTE_REST -1

#define NOTE_C4  261630
#define NOTE_Cs4 277180
#define NOTE_Db4 277180
#define NOTE_D4  293660
#define NOTE_Ds4 311130
#define NOTE_Eb4 311130
#define NOTE_E4  329630
#define NOTE_F4  349230
#define NOTE_Fs4 369990
#define NOTE_Gb4 369990
#define NOTE_G4  392000
#define NOTE_Gs4 415300
#define NOTE_Ab4 415300
#define NOTE_A4  440000
#define NOTE_As4 466160
#define NOTE_Bb4 466160
#define NOTE_B4  493880

#define NOTE_C5  NOTE_C4 *2
#define NOTE_Cs5 NOTE_Cs4*2
#define NOTE_Db5 NOTE_Db4*2
#define NOTE_D5  NOTE_D4 *2
#define NOTE_Ds5 NOTE_Ds4*2
#define NOTE_Eb5 NOTE_Eb4*2
#define NOTE_E5  NOTE_E4 *2
#define NOTE_F5  NOTE_F4 *2
#define NOTE_Fs5 NOTE_Fs4*2
#define NOTE_Gb5 NOTE_Gb4*2
#define NOTE_G5  NOTE_G4 *2
#define NOTE_Gs5 NOTE_Gs4*2
#define NOTE_Ab5 NOTE_Ab4*2
#define NOTE_A5  NOTE_A4 *2
#define NOTE_As5 NOTE_As4*2
#define NOTE_Bb5 NOTE_Bb4*2
#define NOTE_B5  NOTE_B4 *2

#define NOTE_C3  NOTE_C4 /2
#define NOTE_Cs3 NOTE_Cs4/2
#define NOTE_Db3 NOTE_Db4/2
#define NOTE_D3  NOTE_D4 /2
#define NOTE_Ds3 NOTE_Ds4/2
#define NOTE_Eb3 NOTE_Eb4/2
#define NOTE_E3  NOTE_E4 /2
#define NOTE_F3  NOTE_F4 /2
#define NOTE_Fs3 NOTE_Fs4/2
#define NOTE_Gb3 NOTE_Gb4/2
#define NOTE_G3  NOTE_G4 /2
#define NOTE_Gs3 NOTE_Gs4/2
#define NOTE_Ab3 NOTE_Ab4/2
#define NOTE_A3  NOTE_A4 /2
#define NOTE_As3 NOTE_As4/2
#define NOTE_Bb3 NOTE_Bb4/2
#define NOTE_B3  NOTE_B4 /2

#endif /* BUZZER_H_ */
