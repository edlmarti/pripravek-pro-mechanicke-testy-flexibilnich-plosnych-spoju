#include "stepper.h"

static int32_t current_position=0;
static uint8_t current_step=0;
//
static uint64_t max_duty_cyclex1000=0; //1000 => 100%
static uint64_t current_duty_cyclex1000=0; //1000 => 100%

static uint32_t ARR_val=1;

static const uint32_t switching_frequency_Hz = 17000;
static uint32_t prescaler_setting=1;

#undef HALF_STEP

void stepper_setup(uint32_t resistace_mOhm, uint32_t max_current_mA,int32_t supply_voltage_mV)
{
    max_duty_cyclex1000 = max_current_mA/((supply_voltage_mV/resistace_mOhm));

    // ensure GPIOs are enabled
    RCC->AHB1ENR |= 0
                 |  RCC_AHB1ENR_GPIOCEN
                 |  RCC_AHB1ENR_GPIOBEN
                 |  RCC_AHB1ENR_GPIOCEN
                 ;

    // clear motor control pin modes
    GPIOA->MODER &= ~(GPIO_MODER_MODER8 | GPIO_MODER_MODER9);
    GPIOB->MODER &= ~(GPIO_MODER_MODER4 | GPIO_MODER_MODER10);
    GPIOC->MODER &= ~GPIO_MODER_MODER7;

    // set them as outputs
    GPIOA->MODER |= GPIO_MODER_MODER8_0*1 | GPIO_MODER_MODER9_0*1;
    GPIOB->MODER |= GPIO_MODER_MODER4_0*1 | GPIO_MODER_MODER10_0*1;

    GPIOC->MODER |= GPIO_MODER_MODER7_0*1; //Output
    GPIOC->ODR &= ~GPIO_ODR_OD7; //disable motor

    GPIOA->OSPEEDR |= 0
                   |  GPIO_OSPEEDR_OSPEED8_0*3
                   |  GPIO_OSPEEDR_OSPEED9_0*3
                   ;

    GPIOB->OSPEEDR |= 0
                   |  GPIO_OSPEEDR_OSPEED4_0*3
                   |  GPIO_OSPEEDR_OSPEED10_0*3
                   ;

    //GPIOC->AFR[0] |= GPIO_AFRL_AFRL7_0*2;
    //GPIOC->OSPEEDR |= GPIO_OSPEEDR_OSPEED7_0*3;

    // C7 pwm output to enable pins
    timer_setup(TIM3,0); //PWM
    //TIM3->DIER  &= ~TIM_DIER_UIE; // disable update interrupt
    TIM3->DIER  |=  0
                |   TIM_DIER_UIE
                |   TIM_DIER_CC2IE  //enable CC2 interrupt
                ;

    NVIC_SetPriority(TIM3_IRQn, 14); // set high priority for the interrupt
    NVIC_EnableIRQ(TIM3_IRQn); // enable the interrupt

    TIM3->CCMR1 |= 0
//                |  TIM_CCMR1_OC2M_0*6 //CCR sets the on time
//                |  TIM_CCMR1_OC2PE //enable preloading
                ;

    TIM3->CR2   |= TIM_CR2_CCPC;
    //TIM3->CCER  |= TIM_CCER_CC2E; // enable output



    prescaler_setting = 0;
    timer_PSC(TIM3,prescaler_setting);

    //Switching frequency
    ARR_val = sysclk_freq/(switching_frequency_Hz*(prescaler_setting+1))-1;
    timer_ARR(TIM3, ARR_val);

    timer_CCR2(TIM3, ((ARR_val*max_duty_cyclex1000)/1000*900)/1000);
    stepper_current_off();
}
//1000 -> 100%
void stepper_current_throttle(uint32_t percentage_of_maxx1000)
{
    current_duty_cyclex1000 = (max_duty_cyclex1000*percentage_of_maxx1000)/1000;
    //timer_CCR2(TIM3, ((ARR_val*150)/1000));
    //timer_CCR2(TIM3, (ARR_val*(current_duty_cyclex1000)/1000));
}

void stepper_set_home_position()
{
    current_position = 0;
}

void stepper_current_off()
{
    GPIOA->ODR &= ~(GPIO_ODR_OD8 | GPIO_ODR_OD9);
    GPIOB->ODR &= ~(GPIO_ODR_OD4 | GPIO_ODR_OD10);
}

void stepper_back()
{
    current_position++;
    if(current_step >= 3)
        current_step = 0;
    else
        current_step++;
}


void stepper_forward()
{
    current_position--;
    if(current_step == 0)
        current_step = 3;
    else
        current_step--;
}

//uint32_t dbg = 0;
void TIM3_IRQHandler()
{
    if (TIM3->SR & TIM_SR_CC2IF)
        stepper_set_outputs();
    else if(TIM3->SR & TIM_SR_UIF)
        stepper_current_off();
    TIM3->SR = 0;
}

void stepper_set_outputs()
{
    if(system_state == STATE_ERROR)
        stepper_current_off();

    switch (current_step)
    {
    case (0):
    {
        GPIOA->ODR |=  GPIO_ODR_OD8;
        GPIOA->ODR &= ~GPIO_ODR_OD9;

        GPIOB->ODR |=  GPIO_ODR_OD4;
        GPIOB->ODR &= ~GPIO_ODR_OD10;
        break;
    }
    case (1):
    {
        GPIOA->ODR &= ~GPIO_ODR_OD8;
        GPIOA->ODR |=  GPIO_ODR_OD9;

        GPIOB->ODR |=  GPIO_ODR_OD4;
        GPIOB->ODR &= ~GPIO_ODR_OD10;
        break;
    }
    case (2):
    {
        GPIOA->ODR &= ~GPIO_ODR_OD8;
        GPIOA->ODR |=  GPIO_ODR_OD9;

        GPIOB->ODR &= ~GPIO_ODR_OD4;
        GPIOB->ODR |=  GPIO_ODR_OD10;
        break;
    }
    case (3):
    {
        GPIOA->ODR |=  GPIO_ODR_OD8;
        GPIOA->ODR &= ~GPIO_ODR_OD9;

        GPIOB->ODR &= ~GPIO_ODR_OD4;
        GPIOB->ODR |=  GPIO_ODR_OD10;
        break;
    }
    default:
    {
        system_state = STATE_ERROR;
        system_error = ERR_BADSTEP;
        stepper_current_off();
        break;
    }
    }

}

void stepper_on()
{
    GPIOC->ODR |= GPIO_ODR_OD7;
    stepper_current_off();
    timer_enable(TIM3);
}

void stepper_off()
{
    timer_disable(TIM3);
    GPIOC->ODR &= ~GPIO_ODR_OD7;
    stepper_current_off();
}

int32_t stepper_pos()
{
    return current_position;
}

