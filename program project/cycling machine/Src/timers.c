#include "timers.h"

void timer_setup(TIM_TypeDef *TIM,uint8_t CKD_setting)
{
    RCC->APB1ENR |= 0
                 | RCC_APB1ENR_TIM2EN  * (TIM == TIM2)
                 | RCC_APB1ENR_TIM3EN  * (TIM == TIM3)
                 | RCC_APB1ENR_TIM4EN  * (TIM == TIM4)
                 | RCC_APB1ENR_TIM5EN  * (TIM == TIM5)
                 ;

    RCC->APB2ENR |= 0
                 | RCC_APB2ENR_TIM1EN  * (TIM == TIM1)
                 | RCC_APB2ENR_TIM9EN  * (TIM == TIM9)
                 | RCC_APB2ENR_TIM10EN * (TIM == TIM10)
                 | RCC_APB2ENR_TIM11EN * (TIM == TIM11)
                 ;

    TIM->CR1 |= 0
             |  TIM_CR1_CKD_0*CKD_setting // divide input by ckd
             |  TIM_CR1_ARPE  // buffer ARR
             |  TIM_CR1_DIR   // downcounting
          // |  TIM_CR1_OPM  // one pulse mode - timer stops counting at next update event (clears CR1_CEN)
             ;

    TIM->DIER |= TIM_DIER_UIE; // enable update interrupt
}
void timer_onepulse(TIM_TypeDef *TIM, uint8_t onepulse)
{
    if(onepulse)
        TIM->CR1 |= TIM_CR1_OPM;
    else
        TIM->CR1 &= ~TIM_CR1_OPM;
}
void timer_enable(TIM_TypeDef *TIM)
{
    TIM->CR1 |= TIM_CR1_CEN;
}

void timer_disable(TIM_TypeDef *TIM)
{
    TIM->CR1 &= ~TIM_CR1_CEN;
}

void timer_PSC(TIM_TypeDef *TIM, uint32_t PSC)
{
    TIM->PSC = PSC;
}

void timer_ARR(TIM_TypeDef *TIM, uint32_t ARR)
{
    TIM->ARR = ARR;
}

void timer_CCR2(TIM_TypeDef *TIM, uint32_t CCR)
{
    TIM->CCR2 = CCR;
}
