#ifndef DISPLAY_H_
#define DISPLAY_H_

#include "system.h"
#include "font.h"
#include "spi.h"

typedef struct
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
}rgb_t;

extern const rgb_t color_red;
extern const rgb_t color_green;
extern const rgb_t color_blue;
extern const rgb_t color_black;
extern const rgb_t color_white;

// foreground color
extern rgb_t color_fg;
// background color
extern rgb_t color_bg;


// setup the SPI and send inicialization commands
// the display must be woken up and turned on before displaying any data
void display_setup();

// send command by setting DC pin low
void send_command(uint8_t command);

// send data by setting DC pin high
void send_data(uint8_t data);

// send data by setting DC pin high, but send the most significant byte first
void send_data_16b(uint16_t data);

// set DC pin high
void display_set_DC_command();

// set DC pin low
void display_set_DC_data();

// go to sleep
void display_sleep();

// wake the display up
void display_wake_up();

// turn the display on
void display_on();

// turn the display off
void display_off();

// draw a single letter
uint16_t draw_letter_7x5(char ch, uint16_t posx, uint16_t posy, uint16_t scale);

// draw a bordered rectangle
void draw_window(uint16_t posx, uint16_t posy, uint16_t tox, uint16_t toy);

// draw text
void draw_textbox(char *str, uint16_t posx, uint16_t posy, uint16_t tox,
        uint16_t spacingx, uint16_t spacingy, uint16_t scale);

// clear the display with black
void display_clear();

#endif /* DISPLAY_H_ */
