#ifndef TIMERS_H_
#define TIMERS_H_

#include <stdint.h>
#include <stm32f4xx.h>

// setup the timer and its interrupt
void timer_setup(TIM_TypeDef *TIM,uint8_t CKD_setting);

// set or reset one pulse mode for a timer
void timer_onepulse(TIM_TypeDef *TIM, uint8_t onepulse);

// enable the timer
void timer_enable(TIM_TypeDef *TIM);

// disable the timer
void timer_disable(TIM_TypeDef *TIM);

// set prescaler
void timer_PSC(TIM_TypeDef *TIM, uint32_t PSC);

// set autoreload value
void timer_ARR(TIM_TypeDef *TIM, uint32_t ARR);

// set ccr1 value
void timer_CCR2(TIM_TypeDef *TIM, uint32_t CCR1);

#endif /* TIMERS_H_ */
