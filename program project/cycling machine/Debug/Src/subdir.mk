################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/actuator.c \
../Src/buttons.c \
../Src/buzzer.c \
../Src/display.c \
../Src/font.c \
../Src/main.c \
../Src/meas.c \
../Src/spi.c \
../Src/stepper.c \
../Src/syscalls.c \
../Src/sysmem.c \
../Src/system.c \
../Src/timers.c 

OBJS += \
./Src/actuator.o \
./Src/buttons.o \
./Src/buzzer.o \
./Src/display.o \
./Src/font.o \
./Src/main.o \
./Src/meas.o \
./Src/spi.o \
./Src/stepper.o \
./Src/syscalls.o \
./Src/sysmem.o \
./Src/system.o \
./Src/timers.o 

C_DEPS += \
./Src/actuator.d \
./Src/buttons.d \
./Src/buzzer.d \
./Src/display.d \
./Src/font.d \
./Src/main.d \
./Src/meas.d \
./Src/spi.d \
./Src/stepper.d \
./Src/syscalls.d \
./Src/sysmem.d \
./Src/system.d \
./Src/timers.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o Src/%.su: ../Src/%.c Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DSTM32 -DSTM32F4 -DSTM32F411RETx -c -I../Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Src

clean-Src:
	-$(RM) ./Src/actuator.d ./Src/actuator.o ./Src/actuator.su ./Src/buttons.d ./Src/buttons.o ./Src/buttons.su ./Src/buzzer.d ./Src/buzzer.o ./Src/buzzer.su ./Src/display.d ./Src/display.o ./Src/display.su ./Src/font.d ./Src/font.o ./Src/font.su ./Src/main.d ./Src/main.o ./Src/main.su ./Src/meas.d ./Src/meas.o ./Src/meas.su ./Src/spi.d ./Src/spi.o ./Src/spi.su ./Src/stepper.d ./Src/stepper.o ./Src/stepper.su ./Src/syscalls.d ./Src/syscalls.o ./Src/syscalls.su ./Src/sysmem.d ./Src/sysmem.o ./Src/sysmem.su ./Src/system.d ./Src/system.o ./Src/system.su ./Src/timers.d ./Src/timers.o ./Src/timers.su

.PHONY: clean-Src

